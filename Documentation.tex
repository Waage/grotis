Documentation

\section{Introduction}
In order to facillitate rapid trajectory-generation using GROMACS, some modifications were made. These modifications are designed to allow transmission of transition interface information to GROMACS at runtime - without necessitating a complete recompilation of GROMACS by users. This is important for several reasons, primarily:

\begin{itemize}
\item Directly modifying the GROMACS source code can be difficult, as it necessitates relatively deep understanding of a complex source code, spanning multiple files.
\item On a user level, recompiling GROMACS might not be possible, as the installation requires administrator privileges, which are not easily available on e.g. super-computer clusters.
\item The recompilation of GROMACS  is itself a time-consuming process, as the large program size requires a moderately long compilation process.
\end{itemize}

To avoid these issues, the proposed extension defines an abstract class, t_tis_class. Users can pass extensions of this class to GROMACS at runtime, by including a file called \"user_defined_tis_class.so\" in the runfolder of GROMACS. In this manner, a transition interface can be defined and compiled seperately from the main GROMACS installation, allowing for somewhat easier implementation. This document is meant as a documentation of the changes that have been made, as well as a guide of how to use these changes.

\section{Modifications}
In order for GROMACS to implement user defined interfaces, it needs to know some basc rules a-priori. These basic rules are implemented in two files; tis.cpp/.h and tis_class.cpp/.h, both located in src/gromacs/tis. 
\subsection{tis.cpp}
tis.cpp/.h contains the interface between the tis functions and GROMACS. Specifically, the functions initialize_TIS, do_TIS, and finalize_TIS are called by GROMACS' do_md function; at the very beginning, during the simulation, and at the very end. We will describe each function in order of occurence.
	\subsubsection{initialize_TIS}
	This function is called in the function do_md, immediately after the function init_IMD. It is only called if the boolean variable bTIS has been set to true. This function recieves the information that has been transmitted via the input record, ir. It attempts to retrieve the user's definition of a t_tis_class, and, if successfull, creates an object of this class, passing to it information about the \"position\" of the transition interfaces in order parameter-space. In order to retrieve the child class, initialize_TIS calls the local function get_user_defined. This function assigns the tis.cpp-local function pointers create and destroy to the functions create_object and destroy_object, which should be defined in the file user_defined_tis_class.so. These functions are in turn used as constructors and destructors for the t_tis_class.

	\subsubsection{do_TIS}
	This function is called in the function do_md, immediately before the function do_md_trajectory_writing (it might be advantageous to do it immediately after, however?). When called, this function gathers all information about positions and velocities (in case of a multinode simulation), and transmits them to the user-defined update_reaction_coordinate, which computes the order parameter based on this input, and the user's definition of an order parameter. After this, this function stores the reaction coordinate, periodically flushing the storage to the file rxcs.out. If the reaction coordinate has passed either interface (or reached some other, user defined end condition), the function instructs gromacs to end the simulation, by calling gmx_set_stop_condition(gmx_stop_cond_next). Finally, it instructs the tis_class to store the current frame, in such a manner that the first two, and last two frames of each simulation are kept.

	\subsubsection{finalize_TIS}
	This function ensures that all remaining information is printed out at the end of the simulation, by calling the various printing routines defined for the t_tis_class. At the end, it invokes the destructor of the t_tis_class.

\subsection{tis_class.cpp}
tis_class.cpp/.h is the skeleton of the reaction coordinate. It defines which functions \emph{must} be defined by the user, and also specifies some functions that may be defined by the user. It defines default behavior which may be overridden for some functions, and defines immutable functions in other cases. We will first discuss the functions which \emph{must} be defined by the user, continue to the functions that may be defined by users, and end at the functions that can be overridden by users. The immutable functions are not meant to be acted upon by the user, and are left unexplained (they are however, largely, self-explanatory).
	\subsubsection{update_reaction_coordinate}
	Calculates the reaction coordinate based on the current positions and velocities in the system. As it is impossible to tell a-priori what the reaction being considered is, the user is required to supply the definition of this function. It is hoped that this will be easy, as this function \emph{only} recieves information about positions, velocity and box-dimensions, removing any distracting information.

	 \subsubsection{write_specific}
	 By default, this function does nothing. It can be overridden by the user, in order to append specific information of interest to the final output file \"tisfile\", which otherwise only contains information about the highest and lowest reached reaction coordinate, as well as the length of the trajectory, and the state in which the simulation ended.

	 \subsubsection{write_observable}
	 This function is not yet fully implemented, but is intended to allow for storage/output of observable quantities that are not the reaction coordinate, e.g. the deformation of a window-structure during a diffusion process.

	 \subsubsection{get_action}
	 This function determines the state of the simulation, which in turn is used by do_TIS to decide whether the simulation should be terminated. By default, the simulation is to be terminated when either of the transition interfaces has been crossed. The user can override this function to define different termination criteria. In general, if get_action returns an integer, which is greater than zero, the simulation will be terminated.

In addition to these files, some of the original GROMACS files have been altered. Briefly, these are:
\begin{itemize}
\item In the file src/progams/mdrun/md.cpp, do_md has been modified to call the interface functions of tis.cpp, as defined above.
\item In the file src/gromacs/legacyheaders/types/inputrec.h, the abstract type t_tis_class has been forward declared. This is required to allow a pointer to the C++ class of the same name be passed around in C functions. The structure t_TIS has also been defined. This structure carries information about the interfaces from input to the initialize_TIS function. It also passes the pointer to th e tis_class between the initialize_/do_/finalize_ functions. The struct t_inputrec has been modified to contain this t_TIS struct, as well as a boolean bTIS, which defines whether or not the current simulation is a TIS-simulation. Be aware that there also exists a file src/gromacs/legacyheaders/inputrec.h which has \emph{not} been altered.
\item Logic has been added to src/gromacs/gmxpreprocess/readir.c in order for it to check for tis-instructions whe gmx grompp is used. Specifically, it will now search for the line \"tis no/yes\", and, if it is set to yes, the lines left-interface (value) and right-interface(value), which defines the interfaces of the simulation.
\item src/gromacs/fileio/tpxio.c has been altered in several manners. The enum tpxv has had a transition interface member added, in order to distinguish the code from older versions. Also, the tpx_tag has been altered. It is possible that some of these alterations are improper, some input on this matter would be appreciated. Also in this file, the function do_tis has been added. This function is used when reading the interface values, and is called from a segment of do_inputrec which, appropriately, sets the values of the inputrec structure. The style of code applied here conforms to the existing style of the file, and appears to work. It is however, added without complete overview of how the code works, merely invoking the same function calls that are used by code snippets that appear to do the same thing we have atempted to achieve.

