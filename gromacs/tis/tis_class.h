#ifndef TIS_CLASS_H
#define TIS_CLASS_H

#ifdef __cplusplus
#include "gromacs/topology/topology.h"
#include "gromacs/utility/real.h"
#include "gromacs/math/vectypes.h"
#include "gromacs/legacyheaders/typedefs.h"
#include <stdio.h>

typedef struct t_fileio t_fileio;

#define tis_main_observable observable[0]
class t_tis_class
{
public:
	t_tis_class(real upper, real lower,  t_inputrec* ir, gmx_mtop_t *top_global, const char *obsname, const int n_Obs); 	/* Constructor*/
	virtual ~t_tis_class();										    /* Destructor; must be virtual to make 
																    sure that child destructor is also called */
	virtual void update_reaction_coordinate(rvec*, rvec*, rvec*, matrix box) = 0; /*Pure virtual function, to ensure that 
																					  the user supplies an actual implementation
																					  through inheritance */
	void store_reaction_coordinate();								/* Stores the current reaction coordinate into rxc */
	real* get_reaction_coordinate(){return observable;}; /* In case it is needed outside of the class (probably removable) */
	virtual int get_action(int step); /* Actually a bool which is true if the simulaion should be ended, false otherwise. 
										 Side effect: Updates the class' state variable. Should maybe not be virtual? */
	void update_hilo();				  /* Updates the highest/lowest values */
	void store_frame(rvec pos[],rvec vel[], matrix box, int step); /*Stores the configuration of the current step */
	void write_final(FILE *file, int ePBC, gmx_mtop_t *top_global); /* Writes final output before termination of the simulation.*/
	/* Virtual functions for writing specific extra information of a child class. Default: Do nothing. */
	virtual void write_specific(FILE*){ };						/*Called by write_final(). */																	   	
	virtual void write_observable(int){ };						/*Called by TBD */
	int get_update_frequency(){return update_frequency;};

protected:
	real 	  *observable; /* Durr dehurr */
	real 	  upper_interface;	   /* Reaction coordinate value of the upper interface   */
	real 	  lower_interface;	   /* ------------------------- of the lower interface   */	
	real 	  highest;			   /* Highest reached reaction coordinate*/
	real 	  lowest;			   /* Lowest reached reaction coordinate */
	real 	  initial;			   /* Initial reaction coordinate */
	int 	  state;			   /* State of termination. 
								 	  0 = ran out of time, 
								 	  1 = crossed upper interface, 
								 	  2 = crossed lower interface */
	int 	  endstep;			   /* Final step before termination condiion was reached */
	rvec  	**xstore;              /* Positions to be stored */
    rvec  	**vstore;              /* Velocities to be stored */
    matrix	 *boxstore;			   /* Boundary conditions to be stored */
	int natoms;
	/********************* Storage of initial and final steps of configuration ************************/
	int nstore_init;	/*Number of initial steps to store */
	int nstore_end;		/*Number of final steps to store */
	int nstore; /*Total number of steps to store, should probably not be a member variable but whatevs*/
	/**************************************************************************************************/
	/***************************** Storage of reaction coordinates by step ****************************/
	int n_Observables;
	t_fileio *obsfile;
	int update_frequency;


	/**************************************************************************************************/


};
#else
	typedef struct t_tis_class t_tis_class; //Should not really be seen ever, but whatever.
#endif


#endif