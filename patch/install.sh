#!/bin/bash
#

if [ -d "../src/" ]; then
	cp -R src/* ../src/
	cp CMakeLists.txt ..
else
	echo "Did not find src folder of original gromacs distribution!"
fi
