#include "tis.h"
#include "tis_class.h"
#include "gromacs/legacyheaders/sighandler.h"
#include "gromacs/legacyheaders/types/commrec.h"
#include "gromacs/domdec/domdec.h"
#include "gromacs/utility/smalloc.h"
#include <malloc.h>
#include <dlfcn.h>

/* Pointers to dynamically loaded file and functions that define the reaction studied */
static void* user_class_file;
static t_tis_class* (*create)(real, real, t_inputrec*, gmx_mtop_t*, const char*);
static void (*destroy)(t_tis_class*);

void get_user_defined(char *tis_name)
{
	char* err;
	/* Loads the constructor and destructor from a user supplied shared library */
	/* Open the library file */
	user_class_file = dlopen(tis_name, RTLD_LAZY);
	if((err = dlerror()) != NULL)
	{
		fprintf(stderr, "TIS error: Failed to load user defined file %s!\n",tis_name);
		fprintf(stderr, "Error code %s \n",err);
		gmx_set_stop_condition(gmx_stop_cond_abort);
	}
	/* Load the constructor */
	create = (t_tis_class* (*)(real, real, t_inputrec*, gmx_mtop_t*, const char*))dlsym(user_class_file, "create_object");
	if((err = dlerror()) != NULL)
	{
		fprintf(stderr, "TIS error: Failed to load user defined create function from file %s!\n",tis_name);
		fprintf(stderr, "Error code %s \n",err);
		gmx_set_stop_condition(gmx_stop_cond_abort);
	}
	/* Load the destructor */
 	destroy = (void (*)(t_tis_class*))dlsym(user_class_file, "destroy_object");
	if((err = dlerror()) != NULL)
	{
		fprintf(stderr, "TIS error: Failed to load user defined destroy function from file %s!\n",tis_name);
		fprintf(stderr, "Error code %s \n",err);
		gmx_set_stop_condition(gmx_stop_cond_abort);
	}

}

extern void do_TIS(t_commrec *cr, t_inputrec *ir, t_state *state_local, t_state *state_global, rvec *f, rvec *f_global, int step, t_TIS *tis)
{


	if (!ir->nstxout){
		fprintf(stderr, "You should probably print the positions in your trajectory, ya big dumb dumb\n");
		gmx_set_stop_condition(gmx_stop_cond_abort);
	}

	if (PAR(cr))
	{
		/*If the simulation is performed on several nodes, 
		make sure that the master node has access to all (?) required information 
		This solution might make large simulations very inefficient. 
		Possibly let users tune how often this should be done? Or turn it off with some "unsafe" flag option?*/

		if (DOMAINDECOMP(cr))
		{

			dd_collect_vec(cr->dd, state_local, state_local->x, state_global->x);

    		if (ir->nstvout)
            dd_collect_vec(cr->dd, state_local, state_local->v, state_global->v);

    		if (ir->nstfout)
            dd_collect_vec(cr->dd, state_local, f, f_global);
		}
	}

	/* Update the value of the reaction coordinate */
	/* NB: The reaction coordinate for step 0 is also calculated */
	if(MASTER(cr))
	{
		if(!(step % tis->reaction->get_update_frequency()))
		{		
			tis->reaction->update_reaction_coordinate(state_global->x, state_global->v, f_global, state_global->box);

			tis->reaction->store_reaction_coordinate();
			/* Compare new reaction coordinate with maximally/minimally visited reaction coordinate and update those */
			tis->reaction->update_hilo();
			/* Check if we are finished. If we are not already finished, save the current step 
			(reaction->store_frame checks internally if we are finished or not) */
			if(tis->reaction->get_action(step))
			{
				/*If so, tell GROMACS to stop the simulation */
				gmx_set_stop_condition(gmx_stop_cond_next);
			}
			tis->reaction->store_frame(state_global->x,state_global->v,state_global->box,step);
		}
	}

}

extern void initialize_TIS(t_commrec *cr, t_inputrec *ir, gmx_mtop_t *top_global, int nfile, const t_filenm fnm[])
{
	/*Initializes the t_tis_class class by calling the class constructor using the input values from the .mdp file*/
	/*Only the master node needs this */
	if (MASTER(cr))
	{
		
		t_TIS *tis = ir->tis;
		real upper = tis->upper_interface;
		real lower = tis->lower_interface;
		if(lower > upper)
		{
			fprintf(stderr, "TIS error: Interfaces are misaligned.\n");
			gmx_set_stop_condition(gmx_stop_cond_abort);
		}

		//User defined class, to be dynamically loaded.
	    char tis_name[] = "./user_defined_tis_class.so";
		get_user_defined(tis_name);
		tis->reaction = create(upper,lower,ir,top_global, opt2fn("-tisout", nfile, fnm));
		if(!tis->reaction) //Check that the external file returned an allocated pointer
		{
			fprintf(stderr, "TIS error: Failed to create tis class object! Aborting simulation\n");
			gmx_set_stop_condition(gmx_stop_cond_abort);
		}
		fprintf(stderr, "Loaded user defined class\n.");
	}


}

void finalize_TIS(t_commrec *cr, t_inputrec *ir,gmx_mtop_t *top_global)
{
    if (MASTER(cr))
	{
		/* Write output to tisfile, then delete the reaction. Currently only used at the end of the simulation. This needs to be made more properly */
		FILE *tisfile = fopen("tisfile","w+"); /*open file for output. Would this ever be needed during the run? 
												If so, make a member of the t_TIS struct in inputrec.h, and open it in initialize */
		t_TIS *tis = ir->tis;

		/*Write reaction specific information */
		tis->reaction->write_final(tisfile,ir->ePBC,top_global);

		/*Free lots of memory */
		destroy(tis->reaction); //Use the user-supplied destructor
		fclose(tisfile);
		dlclose(user_class_file);
	}
}

