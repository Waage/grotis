#ifndef TIS_H
#define TIS_H
#include "gromacs/legacyheaders/typedefs.h"
#include "gromacs/fileio/filenm.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void do_TIS(t_commrec *cr, t_inputrec *ir, t_state *state_local,t_state *state, rvec *f, rvec *f_global, int step, t_TIS *tis);
extern void initialize_TIS(t_commrec *cr, t_inputrec *ir, gmx_mtop_t *top_global, int nfile, const t_filenm fnm[]);
void finalize_TIS(t_commrec *cr, t_inputrec *ir,gmx_mtop_t *top_global);

#ifdef __cplusplus
}
#endif

#endif