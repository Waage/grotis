#include "tis_class.h"
#include "gromacs/math/vec.h"
#include "gromacs/fileio/gmxfio.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void write_sto_conf_mtop(const char *outfile, const char *title,
                         gmx_mtop_t *mtop,
                         rvec x[], rvec *v, int ePBC, matrix box); 



#ifdef __cplusplus
}
#endif

t_tis_class::t_tis_class(real upper, real lower,  t_inputrec* ir, gmx_mtop_t *mtop, const char *obsname, int n_Obs){
	
	/* Declare the number of atoms studied, define the interfaces */
	natoms = mtop->natoms;
	upper_interface = upper; 
	lower_interface = lower;
	n_Observables = n_Obs;
	state = 0; //Should be changed to an enum?
	/*Ensure that the first reaction coordinate will be the highest and lowest visited */
	highest = lower;
	lowest = upper;
	/* Set the number of initial and final configurations to be stored (currently constant) */
	nstore_init = 2;	
	nstore_end = 2;	
	nstore = nstore_init+nstore_end;
	/*Allocate memory for configuration storage */
	xstore = new rvec*[nstore];
	vstore = new rvec*[nstore];
	boxstore = new matrix[nstore];
	observable = new real[n_Observables];
	for (int i = 0; i < nstore; ++i)
	{
		xstore[i] = new rvec[natoms];
		vstore[i] = new rvec[natoms];
	}
	obsfile = gmx_fio_open(obsname, "w+");
	switch(ir->eTIS){
	case eSpiderman:
		update_frequency = ir->tis->update_freq;
		break;
	case eRetis:
		if(ir->tis->update_freq != ir->nstxout)
		{
			fprintf(stderr, "Warning: Setting tis update frequency equal to nstxout!\n");
		}
		update_frequency = ir->nstxout;
		break;
	default:
		fprintf(stderr, "Error! Unknown TIS option!\n This shouldn't even be possible! Damned useless pollsters.");
	}
}

t_tis_class::~t_tis_class(){
	/* Delete the storage matrices */
	for (int i = 0; i < nstore; ++i)
	{
		delete []xstore[i];
		delete []vstore[i];
	}
	delete [] xstore;
	delete [] vstore;
	delete [] boxstore;
}

void t_tis_class::update_hilo(){
	if(state == 0){ //Only update highest/lowest if we have not reached a termination state
		if(observable[0] > highest) highest = observable[0];
		if(observable[0] < lowest) lowest = observable[0];
	}
}

int t_tis_class::get_action(int step){ 
	//Returns true if we have passed the upper or lower interface
	if(state > 0) return 1; //Do not update the state if we have already reached a termination condition
	endstep = step;
	/*Have we passed the exterior interfaces? */
	if(observable[0] > upper_interface)state = 1;
	if(observable[0] < lower_interface)state = 2;
	return(state > 0);
}

void t_tis_class::store_frame(rvec pos[],rvec vel[], matrix box, int step){
	/*Save first n and last m configurations */
	if(step <= endstep){ //Makes sure we have not yet finished
		if(step < nstore_init){
			/* Just store the step */
			copy_rvecn(pos, xstore[step], 0, natoms);
			copy_rvecn(vel, vstore[step], 0, natoms);
			copy_mat(box,boxstore[step]);
		}else{
			//Some extra logic to overwrite the correct data
			int	storead = ((step-nstore_init)%nstore_end)+nstore_init; //Saves to this address;
			copy_rvecn(pos, xstore[storead], 0, natoms);
			copy_rvecn(vel, vstore[storead], 0, natoms);
			copy_mat(box,boxstore[storead]);	
		}
	}
}

void t_tis_class::write_final(FILE *file, int ePBC, gmx_mtop_t *top_global){
	/* Write information required for analysis */
	fprintf(file, "Lowest reaction coordinate %lf\n",lowest);
	fprintf(file, "Highest reaction coordinate %lf\n",highest);
	fprintf(file, "Last step was %d\n",endstep);
	fprintf(file, "Finished in state %d\n",state);
	write_specific(file); //Default: Does nothing. Can be overwritten to write e.g. interesting observables etc

	char outfilename[14];
	
	/* Write the first n and last m configurations to n+m seperate .g96-files
	   Assumes less than 100 of each */
	for (int i = 0; i < nstore_init; ++i)
	{
		if(endstep<i)break;
		sprintf(outfilename,"staconf%d.g96",i);
		write_sto_conf_mtop(outfilename,outfilename,top_global,xstore[i],vstore[i],ePBC,boxstore[i]);
	}
	for (int i = 0; i < nstore_end; ++i)
	{	if(endstep<i+nstore_init)break;
		sprintf(outfilename,"endconf%d.g96",nstore_end - 1 - i); //Ensures that the last step recieves the highest value
		int j = (endstep - i)%nstore_end + nstore_init; 
		write_sto_conf_mtop(outfilename,outfilename,top_global,xstore[j],vstore[j],ePBC,boxstore[j]);
	}

}

void t_tis_class::store_reaction_coordinate()
{
	if(state<1)
	{
		
		gmx_fio_nwrite_real(obsfile, observable, n_Observables);

	}

	return;
}
