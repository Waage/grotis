using namespace std;
#include <stdlib.h>
#include <cmath>
#include "com_distance.h"


#define com_observable tis_main_observable

com_distance::com_distance(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname) 
 : t_tis_class(upper, lower, ir, top_global, rxcsname, 1) { 

  //Hardcoded values before doing proper input/output stuff
  ng1 = 6;
  ng2 = 24;

    group_1 = new int[ng1];
    group_2 = new int[ng2];
    /* Hardcoded values that should later be read in from an input file */
    int temp1[6] = {56,64,    104,112,        200,208}; //Oxygen atoms of the window
    // int temp2[6] = {56,64,    104,112,        200,208}; //Oxygen atoms of the window
    int temp2[24] ={56,64,72,80,104,112,136,152,168,176,200,208,232,248,264,272,296,304,328,336,344,352,360,368}; //Oxygen atoms of the cage

    int i;
    /* Defines the cage center/water center by listing the water molecules which are used to calculate its center of mass */
    /* Molecules are numbered from 1 to N, while c indexing is from 0 to N-1, hence a shift of -1 */
    for(i = 0;i<ng1;++i){
      group_1[i] = (temp1[i]-1)*4;
    }
    for(i = 0; i < ng2;++i){
      group_2[i] = (temp2[i]-1)*4;
    }
    total_mass_1 = 0.0;
    total_mass_2 = 0.0;
    weight_1 = new real[ng1];
    weight_2 = new real[ng2];
    /*INPUT A WAY OF DETERMINING THE WEIGHT OF EACH ATOM HERE. IF YOU WANT TO USE GROMACS, SCALEMOMENTA.CPP MIGHT BE HELPFUL*/
    for(i = 0;i<ng1;++i){
      weight_1[i] = 1;
      total_mass_1 += weight_1[i];
    }
    for(i = 0;i<ng2;++i){
      weight_2[i] = 1;
      total_mass_2 += weight_2[i];
    }

    fprintf(stderr, "Ended initialization\n");
}

com_distance::~com_distance() {
  delete [] group_1;
  delete [] group_2;
  delete [] weight_1;
  delete [] weight_2;
}

void com_distance::update_reaction_coordinate(rvec pos[], rvec* /*vel*/, rvec* /*force*/, matrix box){

    /* TODO: Fix so that boundary conditions no longer are an issue. Currently assumes they can be ignored */
    double cm1[3], cm2[3]; //Current positions of center of masses of windows/cages

    double norm;
    int i,j,k;

    //Find the positions of the center of masses
    //Assumes boundary conditions are not an issue
    for(i=0; i < 3; ++i){
      cm1[i] = 0.0;
      cm2[i] = 0.0;
    }
    for(i = 0; i < ng1; ++i){
      j = (group_1[i]); 
      cm1[0] = cm1[0] + round(pos[j][0]*1000.0)*0.001*weight_1[i]/total_mass_1; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
      cm1[1] = cm1[1] + round(pos[j][1]*1000.0)*0.001*weight_1[i]/total_mass_1; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
      cm1[2] = cm1[2] + round(pos[j][2]*1000.0)*0.001*weight_1[i]/total_mass_1; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
    }
    for(i = 0; i < ng2; ++i){
      j = (group_2[i]);
      cm2[0] = cm2[0] + round(pos[j][0]*1000.0)*0.001*weight_2[i]/total_mass_2; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
      cm2[1] = cm2[1] + round(pos[j][1]*1000.0)*0.001*weight_2[i]/total_mass_2; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
      cm2[2] = cm2[2] + round(pos[j][2]*1000.0)*0.001*weight_2[i]/total_mass_2; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
    }
    //Define vectors
    com_observable = 0.0;
    for(i=0; i < 3; ++i){
      com_observable += (cm2[i] - cm1[i])*(cm2[i] - cm1[i]);
    }

    com_observable = sqrt(com_observable);
    fprintf(stderr, "%f\n",com_observable);
}
