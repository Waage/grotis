#ifndef com_distance_H
#define com_distance_H
#include <gromacs/tis/tis_class.h>

class com_distance : public t_tis_class{

public:
	com_distance(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname);
  	virtual ~com_distance();
	virtual void update_reaction_coordinate(rvec pos[], rvec vel[], rvec f[], matrix box);

private:

	int *group_1; //List of atoms that define group 1
	int *group_2; //list of atoms that define group 2
	int ng1, ng2; //Number of molecules for 1 and 2
	real *weight_1; //Weight of atoms in group 1
	real *weight_2; //Weight of atoms in group 2
	real total_mass_1;
	real total_mass_2;
};
#endif