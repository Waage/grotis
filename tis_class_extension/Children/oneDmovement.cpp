using namespace std;
#include <stdlib.h>
#include <cmath>
#include "oneDmovement.h"

oneDmovement::oneDmovement(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname) 
 : t_tis_class(upper, lower, ir, top_global, rxcsname, 1) { 

}

oneDmovement::~oneDmovement() {

}

void oneDmovement::update_reaction_coordinate(rvec pos[], rvec* /*vel*/, rvec* /*force*/, matrix box){

		reaction_coordinate = pos[0][0]/box[0][0];
}
