#ifndef oneDmovement_H
#define oneDmovement_H
#include <gromacs/tis/tis_class.h>

class oneDmovement : public t_tis_class{

public:
	oneDmovement(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname);
  	virtual ~oneDmovement();
	virtual void update_reaction_coordinate(rvec pos[], rvec vel[], rvec f[], matrix box);


private:

};
#endif