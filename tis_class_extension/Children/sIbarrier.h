#ifndef sIbarrier_H
#define sIbarrier_H
#include <gromacs/tis/tis_class.h>

class sIbarrier : public t_tis_class{

public:
	sIbarrier(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname);
  	virtual ~sIbarrier();
	virtual void update_reaction_coordinate(rvec pos[], rvec vel[], rvec f[], matrix box);
	virtual void write_observable(int step);


private:

	int diffusingMol; // The molecule which is moving from cage to cage
	int *barlist1; //List of molecules that define cage 1
	int *barlist2; //list of molecules that define cage 2 or the cage window (undecided which is better)
	int ncm1, ncm2; //Number of molecules for 1 and 2
	real *water_weight; //Weight of water atoms
	real *window_distortion; //Actually just measures distance between all pairs of water molecules
	FILE *obsfile;	//This should probably be in the main class as a protected member, which is optionally used in the child-classes, but whatevah

};
#endif