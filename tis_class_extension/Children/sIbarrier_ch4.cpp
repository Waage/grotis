using namespace std;
#include <stdlib.h>
#include <cmath>
#include "sIbarrier.h"


#define ld tis_main_observable
#define wd observable[1] 
#define path_angle observable[2] 


real nearest_image_vector(real x,real ax,real box_x){
	//Return the vector to the closest atom image (in an orthorhombic box)
	real dx = x-ax;
	real half = 0.5*box_x;
	dx = dx>half?dx-box_x:(-dx>half?dx+box_x:dx);
	return dx;
}

sIbarrier::sIbarrier(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname)  //sublime question: why is this not colorcoded?
 : t_tis_class(upper, lower, ir, top_global, rxcsname, 3) { 

	//Hardcoded values before doing proper input/output stuff
	ncm1 = 6;
	ncm2 = 24;

  	barlist1 = new int[ncm1];
  	barlist2 = new int[ncm2];
  	window_distortion = new real[ncm1*(ncm1-1)];
  	/* Weight of water atoms */
  	water_weight = new real(3);
  	water_weight[0] = 16.0/(18.0);
  	water_weight[1] = 1.0/(18.0);
  	water_weight[2] = 1.0/(18.0);

  	/* Hardcoded values that should later be read in from an input file */
  	int temp1[6] = {1,3,48,50,97,144}; //Oxygen atoms of the window
  	int temp2[24] ={1,3,48,50,97,144,191,193,209,211,213,217,241,258,260,265,284,286,302,307,310,334,354,358}; //Oxygen atoms of the cage

  	int i;
  	/* Defines the cage center/water center by listing the water molecules which are used to calculate its center of mass */
  	/* Molecules are numbered from 1 to N, while c indexing is from 0 to N-1, hence a shift of -1 */
  	for(i = 0;i<ncm1;++i){
  		barlist1[i] = temp1[i] -1;
  	}
  	for(i = 0; i < ncm2;++i){
  		barlist2[i] = temp2[i] -1;
  	}
  	int j;
  	int k = 0;
  	for(i = 0;i < (ncm1 - 1);++i){
  		for (j = i + 1; j < ncm1; ++j)
  		{
  			window_distortion[k]=0.0;
  			k++;
  		}
  	}
  	/* Define the diffusing molecule */
  	diffusingMol = 368*4; //Actually atom index
  	obsfile = fopen("window distortion","w+");
  	fprintf(stderr, "Ended initialization\n");
}

sIbarrier::~sIbarrier() {
  delete [] barlist1;
  delete [] barlist2;
  delete [] water_weight;
  delete [] window_distortion;
  fclose(obsfile);
}

void sIbarrier::update_reaction_coordinate(rvec pos[], rvec* /*vel*/, rvec* /*force*/, matrix box){

		double cm1[3], cm2[3]; //Current positions of center of masses of windows/cages

		double cmvec[3], molvec[3]; //Vector from cm1 to cm2, vector from cm1 to molecule defining reaction coordinate
		double norm, norm_mv;
		int i,j,k;

		//Find the positions of the center of masses
		for(i=0; i < 3; ++i){
			cm1[i] = 0.0;
			cm2[i] = 0.0;
			molvec[i] = 0.0;
		}
		//Set an anchor for the center of mass by using the position of the first oxygen atom
		j = (barlist1[0])*4;
		tm = water_weight[0];
		cm1[0] =  pos[j][0]; 
		cm1[1] =  pos[j][1]; 
		cm1[2] =  pos[j][2];

		//Add the mass weight of the hydrogen atoms (might be pointless?)
		for(k = 1; k < 3; ++k){
			tm += water_weight[k];
			cm1[0] = cm1[0] + nearest_image_vector(pos[j+k][0],cm1[0],box[0][0])*water_weight[k]/tm; 
			cm1[1] = cm1[1] + nearest_image_vector(pos[j+k][1],cm1[1],box[1][1])*water_weight[k]/tm; 
			cm1[2] = cm1[2] + nearest_image_vector(pos[j+k][2],cm1[2],box[2][2])*water_weight[k]/tm; 
		}

		//Now loop through the rest of the water molecules
		for(i = 1; i < ncm1; ++i){
			j = (barlist1[i])*4; //There are 4 atoms in each TIP4P water molecule (H2O plus one virtual, massless atom)
			for(k = 0; k < 3; ++k){
				tm += water_weight[k];
				cm1[0] = cm1[0] + nearest_image_vector(pos[j+k][0],cm1[0],box[0][0])*water_weight[k]/tm;
				cm1[1] = cm1[1] + nearest_image_vector(pos[j+k][1],cm1[1],box[1][1])*water_weight[k]/tm;
				cm1[2] = cm1[2] + nearest_image_vector(pos[j+k][2],cm1[2],box[2][2])*water_weight[k]/tm;
			}
		}
		//Now do the same for center of mass computation number two
		j = (barlist2[0])*4;
		tm = water_weight[0];
		cm2[0] =  pos[j][0]; 
		cm2[1] =  pos[j][1]; 
		cm2[2] =  pos[j][2];

		for(k = 1; k < 3; ++k){
			tm += water_weight[k];
			cm2[0] = cm2[0] + nearest_image_vector(pos[j+k][0],cm2[0],box[0][0])*water_weight[k]/tm; 
			cm2[1] = cm2[1] + nearest_image_vector(pos[j+k][1],cm2[1],box[1][1])*water_weight[k]/tm; 
			cm2[2] = cm2[2] + nearest_image_vector(pos[j+k][2],cm2[2],box[2][2])*water_weight[k]/tm; 
		}

		for(i = 1; i < ncm2; ++i){
			j = (barlist2[i])*4; //This is really bad code, btw.
			for(k = 0; k < 3; ++k){
				tm += water_weight[k];
				cm2[0] = cm2[0] + nearest_image_vector(pos[j+k][0],cm2[0],box[0][0])*water_weight[k]/tm;
				cm2[1] = cm2[1] + nearest_image_vector(pos[j+k][1],cm2[1],box[1][1])*water_weight[k]/tm;
				cm2[2] = cm2[2] + nearest_image_vector(pos[j+k][2],cm2[2],box[2][2])*water_weight[k]/tm;
			}
		}

		//Define vectors
		for(i=0; i < 3; ++i){
			cmvec[i] = nearest_image_vector(cm2[i],cm1[i],box[i][i]);
			molvec[i] =nearest_image_vector(pos[diffusingMol][i],cm1[i],box[i][i]); //Assuming only one atom carries the center of mass of the molecule
		}

		norm = 0.0;
 		norm_mv = 0.0;
		ld = 0.0;
		for(i=0; i < 3; ++i){
			ld += cmvec[i]*molvec[i];
			norm += cmvec[i]*cmvec[i];
			norm_mv + = molvec[i]*molvec[i];
		}


		int m1, m2;
		real dx, dy, dz;
		k = 0;
		wd = 0;
	  	for(i = 0;i < (ncm1 - 1);++i){
	  		m1 = barlist1[i]*4;
	  		for (j = i + 1; j < ncm1; ++j)
	  		{
	  			m2 = barlist1[j]*4;
	  			dx = nearest_image_vector(pos[m1][0],pos[m2][0],box[0][0]);
	  			dy = nearest_image_vector(pos[m1][1],pos[m2][1],box[1][1]);
	  			dz = nearest_image_vector(pos[m1][2],pos[m2][2],box[2][2]);
	  			wd += sqrt(dx*dx+dy*dy+dz*dz);
	  			k++;
	  		}
	  	}

		ld /= -sqrt(norm); //I guess we could have uses the gromacs fast inverse square root here if it really mattered
		
		for(i=0; i < 3; ++i){
			cmvec[i] /= sqrt(norm);
			molvec[i] /=sqrt(norm_mv);
		}
		for(i = 0; i < 3; ++i)
		{	
			path_angle += molvec[i]*cmvec[i];
		}
}

void sIbarrier::write_observable(int step)
{
	fprintf(obsfile, "Step %d\n", step);
	for (int i = 0; i < ncm1*(ncm1-1); ++i)
	{
		fprintf(obsfile, "%lf\n", window_distortion[i]);
	}
}
