using namespace std;
#include <stdlib.h>
#include <cmath>
#include "sIbarrier.h"


#define ld tis_main_observable
#define wd observable[1] 
#define angle observable[2] 

sIbarrier::sIbarrier(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname)  //sublime question: why is this not colorcoded?
 : t_tis_class(upper, lower, ir, top_global, rxcsname, 3) { 

	//Hardcoded values before doing proper input/output stuff
	ncm1 = 6;
	ncm2 = 24;

  	barlist1 = new int[ncm1];
  	barlist2 = new int[ncm2];
  	window_distortion = new real[ncm1*(ncm1-1)];
  	/* Weight of water atoms */
  	water_weight = new real(3);
  	water_weight[0] = 16.0/(18.0);
  	water_weight[1] = 1.0/(18.0);
  	water_weight[2] = 1.0/(18.0);

  	/* Hardcoded values that should later be read in from an input file */
  	int temp1[6] = {63,		 103,111,				 199,207}; //Oxygen atoms of the window
  	int temp2[24] ={63,71,79,103,111,135,151,167,175,199,207,231,247,263,271,295,303,327,335,343,351,359,367}; //Oxygen atoms of the cage

  	int i;
  	/* Defines the cage center/water center by listing the water molecules which are used to calculate its center of mass */
  	/* Molecules are numbered from 1 to N, while c indexing is from 0 to N-1, hence a shift of -1 */
  	for(i = 0;i<ncm1;++i){
  		barlist1[i] = temp1[i] -1;
  	}
  	for(i = 0; i < ncm2;++i){
  		barlist2[i] = temp2[i] -1;
  	}
  	int j;
  	int k = 0;
  	for(i = 0;i < (ncm1 - 1);++i){
  		for (j = i + 1; j < ncm1; ++j)
  		{
  			window_distortion[k]=0.0;
  			k++;
  		}
  	}
  	/* Define the diffusing molecule */
  	diffusingMol = 367*4; //Actually atom index
  	obsfile = fopen("window distortion","w+");
  	fprintf(stderr, "Ended initialization\n");
}

sIbarrier::~sIbarrier() {
  delete [] barlist1;
  delete [] barlist2;
  delete [] water_weight;
  delete [] window_distortion;
  fclose(obsfile);
}

void sIbarrier::update_reaction_coordinate(rvec pos[], rvec* /*vel*/, rvec* /*force*/, matrix box){

		/* TODO: Fix so that boundary conditions no longer are an issue. Currently assumes they can be ignored */
		double cm1[3], cm2[3]; //Current positions of center of masses of windows/cages

		double cmvec[3], molvec[3]; //Vector from cm1 to cm2, vector from cm1 to molecule defining reaction coordinate
		double norm;
		int i,j,k;

		//Find the positions of the center of masses
		//Assumes boundary conditions are not an issue
		for(i=0; i < 3; ++i){
			cm1[i] = 0.0;
			cm2[i] = 0.0;
			molvec[i] = 0.0;
		}
		for(i = 0; i < ncm1; ++i){
			j = (barlist1[i])*4; //There are 4 atoms in each TIP4P water molecule (H2O plus one virtual, massless atom)
			for(k = 0; k < 3; ++k){
				cm1[0] = cm1[0] + round(pos[j+k][0]*1000.0)*0.001*water_weight[k]/ncm1; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
				cm1[1] = cm1[1] + round(pos[j+k][1]*1000.0)*0.001*water_weight[k]/ncm1; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
				cm1[2] = cm1[2] + round(pos[j+k][2]*1000.0)*0.001*water_weight[k]/ncm1; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
			}
		}
		for(i = 0; i < ncm2; ++i){
			j = (barlist2[i])*4; //This is really bad code, btw.
			for(k = 0; k < 3; ++k){
				cm2[0] = cm2[0] + round(pos[j+k][0]*1000.0)*0.001*water_weight[k]/ncm2; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
				cm2[1] = cm2[1] + round(pos[j+k][1]*1000.0)*0.001*water_weight[k]/ncm2; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
				cm2[2] = cm2[2] + round(pos[j+k][2]*1000.0)*0.001*water_weight[k]/ncm2; //Doing round(foo*1000)/1000 so calculated result will comply with result obtained by computing from .gro-file
			}
		}
		//Define vectors
		for(i=0; i < 3; ++i){
			cmvec[i] = cm2[i] - cm1[i];
			molvec[i] = round(pos[diffusingMol][i]*1000.0)*0.001 - cm1[i]; //Assuming only one atom carries the center of mass of the molecule
		}
 		//Reaction coordinate: Projection of molvec onto cmvec, i.e. projection of the vector from mass center one to the diffusing molecule 
 		//onto the vector from mass center one to mass center two.
 		norm = 0.0;
		ld = 0.0;
		for(i=0; i < 3; ++i){
			ld += cmvec[i]*molvec[i];
			norm += cmvec[i]*cmvec[i];
		}

		ld /= -sqrt(norm); //I guess we could have uses the gromacs fast inverse square root here if it really mattered

		int m1, m2;
		real dx, dy, dz;
		k = 0;
		wd = 0;
	  	for(i = 0;i < (ncm1 - 1);++i){
	  		m1 = barlist1[i]*4;
	  		for (j = i + 1; j < ncm1; ++j)
	  		{
	  			m2 = barlist1[j]*4;
	  			dx = pos[m1][0] - pos[m2][0];
	  			dy = pos[m1][1] - pos[m2][1];
	  			dz = pos[m1][2] - pos[m2][2];
	  			wd += sqrt(dx*dx+dy*dy+dz*dz);
	  			k++;
	  		}
	  	}

	  	rvec co2_dir;
	  	real norm_co2=0.0;
	  	//C-O vector in co2
		for(i=0; i < 3; ++i){
			co2_dir[i] = round(pos[diffusingMol+1][i]*1000.0)*0.001 - round(pos[diffusingMol][i]*1000.0)*0.001; 
			norm_co2 += co2_dir[i]*co2_dir[i];
		}
		norm_co2 = sqrt(norm_co2);
		for(i=0; i < 3; ++i){
			cmvec[i] /= sqrt(norm);
			co2_dir[i] /=norm_co2;
		}
		for(i = 0; i < 3; ++i)
		{	
			angle += co2_dir[i]*cmvec[i];
		}
		
}

void sIbarrier::write_observable(int step)
{
	fprintf(obsfile, "Step %d\n", step);
	for (int i = 0; i < ncm1*(ncm1-1); ++i)
	{
		fprintf(obsfile, "%lf\n", window_distortion[i]);
	}
}
