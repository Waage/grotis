#include "scalemomenta.h"
#include <math.h>
#include <assert.h>
#include <string>  //This is the c++ string library
#include <string.h> //This is the c string library
#include <sstream>
#include <stdio.h>
#include <cmath>
#include "gromacs/legacyheaders/types/simple.h"

#define COLD(a) a>coldzone_low || a< coldzone_high
#define HOT(a)  a>hotzone_low && a< hotzone_high

//You can do this, but really wouldn't you want to do it properly instead?

void degrees_of_freedom(gmx_mtop_t* top_global, int t_index, real* dof){
	//NB THIS IS BLINDLY COPIED BE CAREFUL!!!!
	/*Haphazardly use the things I think I need from readir.c's function 
	- skipping things I haven't undestood because I maybe won't need them?*/

	//Output: dof: degrees of freedom per atom

	t_atom *atom;
	gmx_moltype_t *molt;
	gmx_molblock_t *molb;
	molb = &top_global->molblock[t_index];
    int ai, aj,  as, imin, jmin;
    t_iatom                *ia;
	int ftype;

	molt = &top_global->moltype[molb->type];
	atom = molt->atoms.atom;
	for (int i = 0; i < molb->natoms_mol; ++i)
	{
		dof[i] = 0;
        if (atom[i].ptype == eptAtom || atom[i].ptype == eptNucleus) dof[i] = 3;
	}
	for (ftype = F_CONSTR; ftype <= F_CONSTRNC; ftype++)
    {
        ia = molt->ilist[ftype].iatoms;
        for (int i = 0; i < molt->ilist[ftype].nr; )
        {
            /* Subtract degrees of freedom for the constraints, rather haphazardly*/
            if (((atom[ia[1]].ptype == eptNucleus) ||
                 (atom[ia[1]].ptype == eptAtom)) &&
                ((atom[ia[2]].ptype == eptNucleus) ||
                 (atom[ia[2]].ptype == eptAtom)))
            {
                dof[ia[1]] -=0.5;
                dof[ia[2]] -=0.5;
            }
            ia += interaction_function[ftype].nratoms+1;
            i  += interaction_function[ftype].nratoms+1;
        }
    }

    /*This deviates from readir.c in a potentially malicious way, thread very carefully!*/
    ia = molt->ilist[F_SETTLE].iatoms;
    for (int i = 0; i < molt->ilist[F_SETTLE].nr; )
    {
	    /* Subtract 1 dof from every atom in the SETTLE */

	    for (int j = 0; j < 3; j++)
	    {
	    	dof[ia[1+j]]--;
	    }
	    i  += 4;
    }

    for (int i = 0; i < molb->natoms_mol; ++i)
	{
    	std::cerr << dof[i] << "\n";
	}


    return;

}

scalemomenta::scalemomenta(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname)  
 : t_tis_class(upper, lower, ir, top_global, rxcsname, 1) { 

	t_profile_file.open("temperature_profile.txt");
	tat_profile_file.open("temperature_profile_atoms.txt");
	endstep = 0;
	nBins = 50;
	T_in_bin = new real[nBins];
	Tat_in_bin = new real[nBins];
	dof_in_bin = new real[nBins];
	nat_in_bin = new real[nBins];
	masses = new real[natoms];
	eta = new rvec[natoms];
	freedom = new real[natoms]; //This is a dumb choice
	coldones = new int[natoms];
	hotones = new int[natoms];
	cold_kin_ener = new rvec[natoms];
	hot_kin_ener = new rvec[natoms];

 	/* Get some info from a supplementary file. Could get most of this from ir or mtop, but I don't really want to pass those to the constructor, tbh. Maybe change that? */
 	std::string dummy;
 	real *temp_mass;

	DeltaQ = upper;
	scriptF = DeltaQ/ir->delta_t;
	delta_t3 = ir->delta_t*ir->delta_t*ir->delta_t;
	n_moltypes = top_global->nmoltype;

	dens_profile_file = new std::ofstream[n_moltypes];
	n_molecules = new int[n_moltypes];
	atoms_per_mol = new int[n_moltypes];
	nmol_in_bin = new int*[n_moltypes];
	int cat = 0;
	char filename[50];
	for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
	{	
		int type_index = top_global->molblock[mt_iter].type;
		strcpy(filename, *top_global->moltype[type_index].name);
		strcat(filename, "_density_profile.txt");
		nmol_in_bin[mt_iter] = new int[nBins];
		dens_profile_file[mt_iter].open(filename);
		std::cerr << filename << '\n';
		n_molecules[mt_iter] = top_global->molblock[mt_iter].nmol;
		atoms_per_mol[mt_iter] = top_global->molblock[mt_iter].natoms_mol;
		real* dof = new real[atoms_per_mol[mt_iter]];
		degrees_of_freedom(top_global, mt_iter, dof);
		for (int k = 0; k < n_molecules[mt_iter]; ++k)
		{
			for (int l = 0; l < atoms_per_mol[mt_iter]; ++l)
			{
				freedom[cat] = dof[l];
				masses[cat] = top_global->moltype[type_index].atoms.atom[l].m;
				cat++;
			}
		}
		delete [] dof;
	}
	//Make sure input isn't hopelessly wrong
	int sum = 0;
	for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
	{
		sum += n_molecules[mt_iter]*atoms_per_mol[mt_iter];
	}

	assert(sum == natoms);

	coldzone_low_rel = (1.0-.5*lower);
	coldzone_high_rel = .5*lower;

	hotzone_low_rel = 0.5 -.5*lower;
	hotzone_high_rel = hotzone_low_rel + lower;

	min_mass =  0.0001;
	tis_main_observable = (highest+lower)/2;
}

scalemomenta::~scalemomenta() {

	delete [] masses;
	delete [] coldones;
	delete [] hotones;
	delete [] cold_kin_ener;
	delete [] hot_kin_ener;
	delete [] T_in_bin;
	delete [] Tat_in_bin;
	delete [] dof_in_bin;
	delete [] nat_in_bin;
	delete [] n_molecules;
	delete [] atoms_per_mol;
	delete [] freedom;
	delete [] eta;
	t_profile_file.close();
	tat_profile_file.close();
	for (int i = 0; i < n_moltypes; ++i)
	{
		dens_profile_file[i].close();
		delete [] nmol_in_bin[i];
	}
	delete [] nmol_in_bin;
	delete [] dens_profile_file;
}

void scalemomenta::update_reaction_coordinate(rvec pos[], rvec* vel, rvec* force, matrix box){

	int ncold, nhot;
	real cold_mass_total, hot_mass_total;
	real x_pos;
	real hot_barycentric_vel[3], cold_barycentric_vel[3];
	real coldzone_low, coldzone_high, hotzone_low, hotzone_high;
	rescale_factor scale;

	//Get the real-space values for the edges of the hot and cold zones
	coldzone_low = coldzone_low_rel*box[0][0];
	coldzone_high = coldzone_high_rel*box[0][0];
	hotzone_low = hotzone_low_rel*box[0][0];
	hotzone_high = hotzone_high_rel*box[0][0];

	ncold=0; nhot=0;
	cold_mass_total = 0.0;
	hot_mass_total = 0.0;
	for (int i = 0; i < 3; ++i)
	{
		
		hot_barycentric_vel[i] = 0.0;
		cold_barycentric_vel[i] = 0.0;
		hot_force[i] = 0.0;
		cold_force[i] = 0.0;
	}
	hot_force_vel = 0.0;
	cold_force_vel = 0.0;
	real x_mass_center;
	real x_anchor;
	real molecule_total_mass;
	int at_index = 0;
	for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
	{		
		for (int k = 0; k < n_molecules[mt_iter]; ++k)
		{
			x_anchor = pos[at_index][0];
			x_mass_center = 0.0;
			molecule_total_mass = 0.0;
			for (int i = at_index; i < at_index + atoms_per_mol[mt_iter]; ++i)
			{
				x_pos = pos[i][0] - x_anchor;
				x_pos = ( (x_pos < -0.5*box[0][0]) ? x_pos + box[0][0] : (x_pos > 0.5*box[0][0]) ? x_pos - box[0][0] : x_pos );
				// "Place" the atom in the box
				x_mass_center += (x_pos+x_anchor)*masses[i];
				molecule_total_mass += masses[i];
			}
			x_mass_center /=molecule_total_mass;
			x_mass_center = ( (x_mass_center < 0.0) ? x_mass_center + box[0][0] : (x_mass_center > box[0][0]) ? x_mass_center - box[0][0] : x_mass_center );
			if (COLD(x_mass_center))
			{
				for (int i = at_index; i < at_index + atoms_per_mol[mt_iter]; ++i)
				{
					coldones[ncold] = i;
					for (int j = 0; j < 3; ++j)
					{	
						cold_kin_ener[ncold][j] = 0.5*masses[i]*vel[i][j]*vel[i][j];
						cold_barycentric_vel[j] += masses[i]*vel[i][j];
					}
					cold_mass_total += masses[i];
					ncold++;
				}
			}
			else if (HOT(x_mass_center))
			{
				for (int i = at_index; i < at_index + atoms_per_mol[mt_iter]; ++i)
				{
					hotones[nhot] = i;
					for (int j = 0; j < 3; ++j)
					{	
						hot_kin_ener[nhot][j] = 0.5*masses[i]*vel[i][j]*vel[i][j];
						hot_barycentric_vel[j] += masses[i]*vel[i][j];
					}
					hot_mass_total += masses[i];
					nhot++;
				}
			}
			at_index += atoms_per_mol[mt_iter];
		}
	}
	for (int i = 0; i < 3; ++i)
	{
		cold_barycentric_vel[i] /= cold_mass_total;
		hot_barycentric_vel[i] /= hot_mass_total;
	}

	scale = getscale(hot_barycentric_vel,cold_barycentric_vel,cold_kin_ener,hot_kin_ener, cold_mass_total, hot_mass_total, ncold, nhot);

	/*Splitting error is evaluated before updating velocities (?)*/
	for (int i = 0; i < ncold; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			eta[i][j] = -0.5*scriptF/scale.kcold*(vel[coldones[i]][j]-cold_barycentric_vel[j]);
			cold_force[j] += force[coldones[i]][j];
			cold_force_vel += force[coldones[i]][j]*(vel[coldones[i]][j]-cold_barycentric_vel[j]);
		}
	}

	for (int i = 0; i < nhot; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			eta[i][j] = 0.5*scriptF/scale.khot*(vel[hotones[i]][j]-hot_barycentric_vel[j]);
			hot_force[j] += force[hotones[i]][j];
			hot_force_vel += force[hotones[i]][j]*(vel[hotones[i]][j]-hot_barycentric_vel[j]);
		}
	}


	for (int i = 0; i < ncold; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vel[coldones[i]][j] = scale.xicold*vel[coldones[i]][j] + (1.0 - scale.xicold)*cold_barycentric_vel[j];
			if(masses[coldones[i]]>min_mass){
				pos[coldones[i]][j] -= delta_t3*( eta[i][j]/scale.kcold*(-scriptF/48.0+1.0/6.0*cold_force_vel)
									 +scriptF/(12.0*scale.kcold)*(force[coldones[i]][j]/masses[coldones[i]] - 1.0/cold_mass_total*cold_force[j]));
			}
		}
	}

	for (int i = 0; i < nhot; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vel[hotones[i]][j] = scale.xihot*vel[hotones[i]][j] + (1.0 - scale.xihot)*hot_barycentric_vel[j];
			if(masses[hotones[i]]>min_mass){
				pos[hotones[i]][j] -= delta_t3*( eta[i][j]/scale.khot*(scriptF/48.0+1.0/6.0*hot_force_vel)
									 -scriptF/(12.0*scale.khot)*(force[hotones[i]][j]/masses[hotones[i]] - 1.0/hot_mass_total*hot_force[j]));
			}
		}
	}

	if((endstep%100) == 0){
		/* Compute the temperature in N slabs along the x-axis, print to file */
		// T = sum(m*v^2)/(R*#DOF), [m] = g/mol, [v] = 10^3 m/s, => [R] = kJ/K mol gives T in Kelvin
		for (int j = 0; j < nBins; ++j)
		{
			T_in_bin[j] = 0.0;
			Tat_in_bin[j] = 0.0;
			dof_in_bin[j] = 0.0;
			nat_in_bin[j] = 0;
			// v_in_bin[j] = 0.0;
			// m_in_bin[j] = 0.0;
			for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
			{
				nmol_in_bin[mt_iter][j] = 0;
			}
		}
		at_index = 0;
		// for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
		// {	
		// 	for (int k = 0; k < n_molecules[mt_iter]; ++k)
		// 	{
		// 		v_in_bin[j][0] += masses[i]*(vel[i][0]);
		// 		v_in_bin[j][1] += masses[i]*(vel[i][1]);
		// 		v_in_bin[j][2] += masses[i]*(vel[i][2]);
		// 		m_in_bin[j] += masses[i];
		// 	}
		// }
		// v_in_bin[j][0]/=m_in_bin[j];
		// v_in_bin[j][1]/=m_in_bin[j];
		// v_in_bin[j][2]/=m_in_bin[j];
		at_index = 0;
		for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
		{	
			for (int k = 0; k < n_molecules[mt_iter]; ++k)
			{
				x_anchor = pos[at_index][0];
				x_mass_center = 0.0;
				molecule_total_mass = 0.0;
				for (int i = at_index; i < at_index + atoms_per_mol[mt_iter]; ++i)
				{
					x_pos = pos[i][0] - x_anchor;
					x_pos = ( (x_pos < -0.5*box[0][0]) ? x_pos + box[0][0] : (0.5*x_pos > box[0][0]) ? x_pos - box[0][0] : x_pos );
					// "Place" the atom in the box
					x_mass_center += (x_pos+x_anchor)*masses[i];
					molecule_total_mass += masses[i];
					int j = pos[i][0] < 0.0 ? floor(nBins*pos[i][0]/box[0][0] + nBins) : pos[i][0] > box[0][0] ? floor(nBins*pos[i][0]/box[0][0] - nBins) : floor(nBins*pos[i][0]/box[0][0]);
					Tat_in_bin[j] += masses[i]*(vel[i][0]*vel[i][0] + vel[i][1]*vel[i][1] + vel[i][2]*vel[i][2]);
					// ((vel[i][0]-v_in_bin[j][0])*(vel[i][0]-v_in_bin[j][0])
					// 						   +(vel[i][1]-v_in_bin[j][1])*(vel[i][1]-v_in_bin[j][1])
					// 						   +(vel[i][2]-v_in_bin[j][2])*(vel[i][2]-v_in_bin[j][2]));
					nat_in_bin[j] += freedom[i];
				}
				x_mass_center /= molecule_total_mass;
				x_mass_center = ( (x_mass_center < 0.0) ? x_mass_center + box[0][0] : (x_mass_center > box[0][0]) ? x_mass_center - box[0][0] : x_mass_center );
				int j = floor(nBins*x_mass_center/box[0][0]);
				
				nmol_in_bin[mt_iter][j]++; 
				for (int i = at_index; i < at_index + atoms_per_mol[mt_iter]; ++i)
				{
					dof_in_bin[j] += freedom[i];
					T_in_bin[j] += masses[i]*(vel[i][0]*vel[i][0]+vel[i][1]*vel[i][1]+vel[i][2]*vel[i][2]);
				}

				at_index += atoms_per_mol[mt_iter];
			}
		}
		for (int j = 0; j < nBins; ++j)
		{
			t_profile_file << T_in_bin[j]/(0.008314*(dof_in_bin[j]-3.0)) << " "; //Temperature in each bin in Kelvin
			tat_profile_file << Tat_in_bin[j]/(0.008314*(nat_in_bin[j]-3.0)) << " "; //Temperature in each bin in Kelvin - atom based, neglect shake/rattle etc
			for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
			{
				dens_profile_file[mt_iter] << nmol_in_bin[mt_iter][j]  << " ";
			}
		}
		t_profile_file << "\n";
		tat_profile_file << "\n";
		for (int mt_iter = 0; mt_iter < n_moltypes; ++mt_iter)
		{
			dens_profile_file[mt_iter] << "\n";
		}
	}
	
		
}

rescale_factor scalemomenta::getscale(real hot_barycentric_vel[3], real cold_barycentric_vel[3], rvec* cold_kin_ener, rvec* hot_kin_ener, 
									 real cold_mass_total, real hot_mass_total, int ncold, int nhot)
{
	/* HEX algorithm [Mol. Phys. 80, 1389 (1993); 81, 251 (1994)], at least according to eHEX paper by Wirnsberger et al http://dx.doi.org/10.1063/1.4931597 */
	/*eh, lets do eHEX anyway */
	rescale_factor output;
	output.khot = 0.0; 
	output.kcold = 0.0;

	for (int i = 0; i < 3; ++i)
	{
		//add up all kinetic energies in both zones
		for (int j = 0; j < ncold; ++j)
		{
			output.kcold += cold_kin_ener[j][i];
		}
		for (int j = 0; j < nhot; ++j)
		{
			output.khot += hot_kin_ener[j][i];
		}
		//Subtract center of mass kinetic energy in each zone
		output.kcold -= 0.5*cold_mass_total*cold_barycentric_vel[i]*cold_barycentric_vel[i];
		output.khot -= 0.5*hot_mass_total*hot_barycentric_vel[i]*hot_barycentric_vel[i];
	}
	if(output.kcold<=0.0001 || output.khot <= 0.000001){
		output.xicold=1.0;output.xihot=1.0;
	}else{
		if(DeltaQ > output.kcold){
			output.xicold = sqrt(1.0 - output.kcold/output.kcold);
			output.xihot = sqrt(1.0 + output.kcold/output.khot);
		}else{
			output.xicold = sqrt(1.0 - DeltaQ/output.kcold);
			output.xihot = sqrt(1.0 + DeltaQ/output.khot);
		}


	}

	return output;

}

int scalemomenta::get_action(int step){ 
	/* Overriding get_action in order to avoid self-termination - also avoids some uninitialized value things */
	state = 0;
	endstep = step;
	return 0;
}

void scalemomenta::write_observable(int step){


}

