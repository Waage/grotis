#ifndef scalemomenta_H
#define scalemomenta_H
#include <gromacs/tis/tis_class.h>
#include <iostream>
#include <fstream>

typedef struct{
	real xihot;
	real xicold;
	real khot;
	real kcold;
}rescale_factor;

class scalemomenta : public t_tis_class{

public:
	scalemomenta(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname);
  	virtual ~scalemomenta();
	virtual void update_reaction_coordinate(rvec pos[], rvec vel[], rvec f[], matrix box);
	virtual void write_observable(int step);
	virtual rescale_factor getscale(real hot_vel_total[3], real cold_vel_total[3], rvec* cold_kin_ener, rvec* hot_kin_ener, 
									 real cold_mass_total, real hot_mass_total, int ncold, int nhot);
	int get_action(int step);

private:
	real min_mass;
	int nBins;
	real *masses;
	real *freedom;
	int *coldones, *hotones;
	rvec *cold_kin_ener;
	rvec *hot_kin_ener;
	rvec cold_force, hot_force;
	real cold_force_vel, hot_force_vel;
	real coldzone_low_rel;
	real coldzone_high_rel;
	real DeltaQ;
	real scriptF; //c.f. eHEX algorithm dunno what to call it
	real delta_t3;
	rvec *eta;
	rvec *splittingError;

	int n_moltypes;
	int *n_molecules;
	int *atoms_per_mol;

	real hotzone_low_rel;
	real hotzone_high_rel;
	real *T_in_bin;
	real *v_in_bin;
	real *Tat_in_bin;
	real *dof_in_bin;
	real *nat_in_bin;
	int **nmol_in_bin;

	std::ofstream t_profile_file;
	std::ofstream tat_profile_file;
	std::ofstream *dens_profile_file;
	std::ifstream hexfile;
	
};
#endif