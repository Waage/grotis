switchmomentum::switchmomentum(real upper, real lower, gmx_mtop_t* top_global, const char *rxcsname)
 : t_tis_class(upper, lower, ir, top_global, rxcsname, 1) { 

	masses = new real[nats];
	coldones = new int[nats];
	hotones = new int[nats];

	/*Determines the relative positions of the hot and cold zones
	 Should look as follows
	 |--|-------------|--|--------------|
	 |  |             |  |			    |
	 | C|             | H|			    |
	 | O|             | O|			    |
	 | L|             | T| 			    |
	 | D|             |  |			    |
	 |  |             |  |			    |
	 |--|-------------|--|--------------|
	 */

	coldzone_low_rel = 0.0;
	coldzone_high_rel = 0.1;

	hotzone_low_rel = 0.45;
	hotzone_high_rel = 0.55;


}

switchmomentum::~switchmomentum() {

	delete [] masses;
	delete [] coldones;
	delete [] hotones;
	delete [] coldvel;
	delete [] hotvel;

}

void switchmomentum::update_reaction_coordinate(rvec pos[], rvec vel[], t_inputrec* ir, rvec* /*force*/, matrix box){
	/*Hi-jacks the normal tis-patch function in order to instead mess with velocities*/
	/*This is certainly safe and will not cause any harmful behavior*/
	/*Swaps the momenta of N cold/hot molecules */

	int ncold, nhot;
	int cold_swap_list_index, hot_swap_list_index;
	int cold_swap_atoms_index, hot_swap_atoms_index;
	real kincold, kinhot;
	real coldzone_low, coldone_high, hotzone_low, hotzone_high;

	//Get the real-space values for the ends of the hot and cold zones
	coldzone_low = coldzone_low_rel*box[0][0];
	coldzone_high = coldzone_high_rel*box[0][0];
	hotzone_low = hotzone_low_rel*box[0][0];
	hotzone_high = hotzone_high_rel*box[0][0];
	
	ncold=0; nhot=0;

	for (int i = 0; i < nats; ++i)
	{
		if (pos[i][0]>coldzone_low && pos[i][0]> coldzone_high)
		{
			coldones[ncold] = i;
			ncold++;
		}
		if (pos[i][0]>hotzone_low && pos[i][0]> hotzone_high)
		{
			hotones[nhot] = i;
			nhot++;
		}
	}

	
	for (int i = 0; i < n_swaps; ++i)
	{
		/* Choose a random cold molecule*/
		cold_swap_list_index = random_number()*ncold;
		hot_swap_list_index = random_number()*nhot;

		cold_swap_atoms_index = coldones[cold_swap_list_index];
		hot_swap_atoms_index = hotones[hot_swap_list_index];

		swap_vecval(vel[cold_swap_atoms_index], vel[hot_swap_atoms_index],3);

		/*Make sure these atoms do not swap momenta with any others */
		coldones[cold_swap_list_index] = coldones[ncold];
		hotones[hot_swap_list_index] = hotones[nhot];
		ncold--;
		nhot--;

	}

	
		
}

void swap_vecval(real *vec1, real *vec2, int dim){
	real tmp;
	for (int i = 0; i < dim; ++i)
	{
		tmp = vec1[i];
		vec1[i] = vec2[i];
		vec2[i] = tmp;
	}
}
