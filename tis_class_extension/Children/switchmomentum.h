#ifndef switchmomentum_H
#define switchmomentum_H
#include <gromacs/tis/tis_class.h>

class switchmomentum : public t_tis_class{

public:
	switchmomentum(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname);
  	virtual ~switchmomentum();
	virtual void update_reaction_coordinate(rvec pos[], rvec vel[], rvec f[], matrix box);
	virtual void write_observable(int step);


private:

	real *masses;
	int *coldones, hotones;

	real coldzone_low_rel, coldone_high_rel, hotzone_low_rel, hotzone_high_rel;
	
	
};
#endif