#include <numpy/npy_no_deprecated_api.h>
#include "use_python_rx.h"
#include <stdlib.h>
#include <numpy/arrayobject.h>
#include <object.h> 
#define PYCONSTRUCT "constructor"
#define PYDESTRUCT "destructor"
#define PYRXC "reaction"

/* A very untested code that (emphasis) should allow you to call a python defined reaction coordinate instead of having to use compiled c */

int init_numpy(){

    // PyRun_SimpleString("import numpy");
    // PyRun_SimpleString("print(numpy.version.version)");
    if (PyImport_ImportModule("numpy.core.multiarray") == NULL)
    PyErr_Print();
	import_array1(-1)
	return 0;
}

use_python_rx::use_python_rx(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname)
 : t_tis_class(upper, lower, ir, top_global, rxcsname, 1) { 

 	/*Initialize the python module */
 	// setenv("PYTHONPATH", ".", 1);
    Py_Initialize();    
    if(init_numpy()){
		fprintf(stderr,"Error importing numpy!\n");
		fprintf(stderr,"Make sure numpy is installed and up to date!\n");
    }
    PyRun_SimpleString("import sys");
	PyRun_SimpleString("import os");
	PyRun_SimpleString("sys.path.append('.')");
	PyRun_SimpleString("print('Current working dir : %s' % os.getcwd())");
	PyObject* pName = PyUnicode_DecodeFSDefault("pyTIS");
	pModule = PyImport_Import(pName); //Import the module
	if(pModule==NULL){
		fprintf(stderr,"Error importing python module!\n");
	}

	pConstruct = PyObject_GetAttrString(pModule, PYCONSTRUCT);
	if(pConstruct==NULL){
		fprintf(stderr,"Error importing constructor!\n");
	}
	pDestruct = PyObject_GetAttrString(pModule, PYDESTRUCT);
	if(pDestruct==NULL){
		fprintf(stderr,"Error importing destructor!\n");
	}
	pReaction = PyObject_GetAttrString(pModule, PYRXC);
	if(pReaction==NULL){
		fprintf(stderr,"Error importing reaction!\n");
	}
	/* Now we call the constructor. Note that this call has an empty argument list */
	PyObject_CallObject(pConstruct, NULL); //Call pConstruct with no arguments and no output.

	Py_DECREF(pName); //We are done with pName, make sure it doesn't stick around.
}

use_python_rx::~use_python_rx() {

	PyObject_CallObject(pDestruct, NULL); //Call the destructor in python

	/* Dereference the PyObjects */
	Py_CLEAR(pReaction);
	Py_CLEAR(pConstruct);
	Py_CLEAR(pDestruct);
	Py_CLEAR(pModule);
	/* Delete the PyObjects */
	delete pReaction;
	delete pConstruct;
	delete pDestruct;
	delete pModule;

}

void use_python_rx::update_reaction_coordinate(rvec pos[], rvec vel[], rvec* force, matrix box){

	npy_intp posdim[2];
	npy_intp veldim[2]={natoms,3};
	npy_intp boxdim[2]={3,3};
	posdim[0]=natoms;
	posdim[1]=3;
	int *x = new int[4];
	x[0]=1;x[1]=2;x[2]=3;x[3]=4;
	npy_intp dims[1];
	dims[0] = 4;
	/* Potential pitfalls: I am claiming that pos, vel and box are float.
	   However, they are type real, which sometimes is float and sometimes not?
	   This may well come back to bite everyone in the ass, repeatedly */
	fprintf(stderr, "Put the pressure on them, homie\n");
	PyObject* PyPos = PyArray_SimpleNewFromData(1, dims, NPY_INT64, (void*) x);
	fprintf(stderr, "Put the pressure on them\n");
	// PyObject* PyPos = PyArray_SimpleNewFromData(2, posdim, NPY_FLOAT, (void*) pos);
	// PyObject* PyVel = PyArray_SimpleNewFromData(2, veldim, NPY_FLOAT, (void*) vel);
	// PyObject* PyFor = PyArray_SimpleNewFromData(2, veldim, NPY_FLOAT, (void*) force);
	// PyObject* PyBox = PyArray_SimpleNewFromData(2, boxdim, NPY_FLOAT, (void*) box);

	// PyObject* args = PyTuple_Pack(4,PyPos, PyVel, PyFor, PyBox);
	// PyObject* rxc = PyObject_CallObject(pReaction, args);
	// observable[0] = (real) PyFloat_AsDouble(rxc); //Might be some conversion issues here, who knows?!

	// /* Make sure there's no memory leaking */
	// Py_CLEAR(rxc);
	// Py_CLEAR(args);
	// Py_CLEAR(PyBox);
	// Py_CLEAR(PyVel);
	// Py_CLEAR(PyFor);
	Py_CLEAR(PyPos);
		
}