#ifndef use_python_rx_H
#define use_python_rx_H
#include <gromacs/tis/tis_class.h>
#include <Python.h>


class use_python_rx : public t_tis_class{

public:
	use_python_rx(real upper, real lower, t_inputrec* ir, gmx_mtop_t* top_global, const char *rxcsname);
  	virtual ~use_python_rx();
	virtual void update_reaction_coordinate(rvec*, rvec*, rvec* , matrix);


private:

	PyObject* pModule;      /* The python module to be imported */
	PyObject* pReaction;	/* The reaction function */
	PyObject* pConstruct;	/* The constructor call */
	PyObject* pDestruct; 	/* The destructor call */

};
#endif