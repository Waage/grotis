#ifndef _CLASSNAME
#error "_CLASSNAME must be defined"
#endif
#ifdef _HEADER
#include _HEADER
#else
#error "_HEADER must be defined"
#endif
#include <gromacs/tis/tis_class.h>
#ifdef __cplusplus
extern "C"{
#endif

extern t_tis_class* create_object(real upper, real lower,  t_inputrec* ir, gmx_mtop_t* top_global, const char* rxcsname)
{
  return new _CLASSNAME(upper,lower, ir, top_global, rxcsname);
}

extern void destroy_object(t_tis_class* object)
{
  delete object;
}

#ifdef __cplusplus
}
#endif